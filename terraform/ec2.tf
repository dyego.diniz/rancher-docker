resource "aws_key_pair" "chave-tf-1" {
  key_name   = "chave-tf-1"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDO+kBKNX1XRahpE+Oa0cydV8fnb72fXLGt4h3+aMoX7Mm/7sfs650zI18H4GI5RtN8ZL45YwZIsl6NzKtkrxeOx1f40WncRihqIuBVdqAfruPnPeHyF1mVQSbc3Vjqi8vG8XjIUfIvmWFBzuQNaCd/x1EKT/lncxb555jOo7uFDr5kYWnQ78hWExsDyW/Ngcfxwy+IC/kWWRh65Xbf/2mIZ+94MIeYYC4L5Ux44fJy3uk54BUIIRSSpmDyKH/IAasyX2U3nBviI3/A5VI96nTfVnf5Es4vTjqGE98cpYyHF0G3x/ZCKZ05oeim+QjuEM4X/c39ZdKRIim95XB6YW8J ubuntu"
}

resource "aws_instance" "vm" {
  ami           = var.instance_ami
  instance_type = var.instance_type
  key_name = aws_key_pair.chave-tf-1.key_name
  subnet_id = aws_subnet.publicsubnets.id
  associate_public_ip_address = true
  private_ip = "10.22.1.201"
  vpc_security_group_ids = [aws_security_group.ec2-sg.id]
  
  provisioner "file" {
    source      = "certificado"
    destination = "/tmp"
  }
   connection {
    type     = "ssh"
    user     = "ubuntu"
    #password = "${rsadecrypt(aws_instance.vm.password_data, file("chave-tf-1.pem"))}" #var.root_password
    private_key = file("chave-tf-1.pem")
    host     = aws_instance.vm.public_ip #self.public_ip
  }
  # provisioner "file" {
  #   source      = "/certificado/"
  #   destination = "/tmp"
  # }
  # provisioner "file" {
  #   source      = "dyego-cloud-privkey.pem"
  #   destination = "/tmp/dyego-cloud-privkey.pem"
  # }

  # provisioner "file" {
  #   source      = "~/certificado"
  #   destination = "~/certificado"

  #   connection {
  #     type        = "ssh"
  #     user        = "ubuntu"
  #     private_key = "${file("chave-tf-1.pem")}"
  #     host        = "${self.public_dns}"
  #   }
  # }

  user_data = "${file("script-inicial.sh")}"
  tags = {
    "Name" = "ec2-tf-srv02"
    "deploy" = "terraformdeploy"
  }
}