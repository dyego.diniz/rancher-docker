# vpc.tf
variable "region" {
  type = string
  description = "regiao da aws"
  default = "us-east-1"
}

variable "main_vpc_cidr" {
  type = string
  description = "vpc cidr"
  default = "10.22.0.0/16"
}

variable "public_subnets" {
  type = string
  description = "cidr publico"
  default = "10.22.1.0/24"
}

variable "private_subnets" {
  type = string
  description = "cidr privado"
  default = "10.22.2.0/24"
}

variable "public_ip" {
  type = string
  description = "meu ip público"
  default = "177.202.171.238/32" 
}

# ec2.tf
variable "instance_ami" {
  type = string
  description = "ami for instance in aws"
  default = "ami-08d4ac5b634553e16" 
}

variable "instance_type" {
  type = string
  description = "type of instance on aws"
  default = "t3.medium"  
}