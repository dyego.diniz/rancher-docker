# Project name: rancher-docker

# objectives
This project show how to install rancher using docker

# necessary projects dependencies
- 

# requisites
- 

# to do
- install rancher using script file
- install certmanager
- install ingress controller
- create outputs for terraform


# doing

# done
- create a valid certificate
- create the terraform files to up the project on AWS

# improvements
- 

# references
- https://rancher.com/docs/rancher/v2.5/en/installation/other-installation-methods/single-node-docker/
- 

# commands
- terraform apply
- you need to create a DNS, I'm using route 53
- access https://[your-dns]
